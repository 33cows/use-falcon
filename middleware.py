# -*- coding: utf-8 -*-
"""
Middleware for aggrostudios webhooks
"""

import json


class ParseJSON(object):
    def __init__(self, http_bad_request):
        self.http_bad_request = http_bad_request

    def process_request(self, req, resp):
        if not req.content_length:
            return
        body = req.stream.read()
        try:
            req.context['data'] = json.loads(body.decode('utf-8'))
        except (ValueError, UnicodeDecodeError):
            raise self.http_bad_request('Bad request', 'JSON was incorrect or not encoded as UTF-8.')
