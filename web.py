#!/usr/bin/env python
import falcon
import handler
from middleware import ParseJSON

# Init application
app = falcon.API(
    middleware=ParseJSON(falcon.HTTPBadRequest)
)


# Routes
app.add_route('/asana/{bot_id}', handler.AsanaCreateComment())
