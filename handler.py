# -*- coding: utf-8 -*-
"""
Handlers for aggrostudios webhooks
"""

import asana
import re

from config import ROBOTS


class AsanaCreateComment(object):

    def on_post(self, req, resp, bot_id):
        client = asana.Client.basic_auth(ROBOTS.get(bot_id))
        pattern = re.compile('https?://.{0,5}asana\.com/[^ ]*/(\d+)/?f?', re.M | re.S)
        commits = req.context.get('data').get('commits', [])
        for commit in commits:
            tasks = pattern.findall(commit['message'])
            for task_id in tasks:
                message = re.sub('https?://.{0,5}asana\.com/[^ ]*/%s/?f?' % task_id,
                                 '@', commit['message'])
                client.stories.create_on_task(task_id, text='%s\n%s: %s' % (
                    commit['url'],
                    commit['author']['name'],
                    message.strip()))
